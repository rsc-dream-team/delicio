# frozen_string_literal: true

Rails.application.routes.draw do
  devise_for :users, controllers: { registrations: 'user_registrations' }
  resources :articles
  resources :topics
  resources :allergies
  resources :diets
  resources :feedbacks
  get '/about_us', to: 'about_us#index'
  resource :user_profiles
  resource :locales

  root to: 'articles#index'
  resources :recipes, :products
end
