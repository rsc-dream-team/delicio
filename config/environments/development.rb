Rails.application.configure do
  default_web_protocol  = 'https'
  config.cache_classes = false

  config.eager_load = false
  config.consider_all_requests_local = true

  if Rails.root.join('tmp', 'caching-dev.txt').exist?
    config.action_controller.perform_caching = true

    config.cache_store = :memory_store
    config.public_file_server.headers = {
      'Cache-Control' => "public, max-age=#{2.days.to_i}"
    }
  else
    config.action_controller.perform_caching = false
    config.cache_store                       = :null_store
  end

  config.active_storage.service = :local
  config.action_mailer.raise_delivery_errors = true
  config.action_mailer.perform_caching = false

  config.active_support.deprecation = :log
  config.active_record.migration_error = :page_load
  config.active_record.verbose_query_logs = true
  config.assets.debug = true
  config.assets.quiet = true
  config.action_view.raise_on_missing_translations = true
  config.file_watcher = ActiveSupport::EventedFileUpdateChecker

  config.action_mailer.perform_deliveries    = true
  config.action_mailer.raise_delivery_errors = true
  config.action_mailer.delivery_method       = :smtp

  # SMTP settings for mailgun
  ActionMailer::Base.smtp_settings = {
    port:            ENV['MAILGUN_PORT'],
    address:         ENV['MAILGUN_ADDRESS'],
    domain:          ENV['MAILGUN_DOMAIN'],
    user_name:       ENV['MAILGUN_USERNAME'],
    password:        ENV['MAILGUN_PASSWORD'],
    authentication:  :plain,
  }

  config.action_mailer.default_url_options = {
    host:     ENV['WEB_DOMAIN'],
    protocol: ENV.fetch('WEB_PROTOCOL', default_web_protocol)
  }
end
