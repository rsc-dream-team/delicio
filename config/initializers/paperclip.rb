# frozen_string_literal: true

return if Rails.env.test?

require 'aws-sdk-s3'

Paperclip::Attachment.default_options.merge!(
  storage:        :s3,
  preserve_files: true,
  s3_region:      ENV['AWS_REGION'],
  s3_protocol:    :https,
  s3_host_name:   "s3-#{ENV['AWS_REGION']}.amazonaws.com",
  path:           "/#{Rails.env}/:class/:id/:style/:basename.:extension",
  s3_credentials: {
    bucket:            ENV['S3_BUCKET_NAME'],
    access_key_id:     ENV['AWS_ACCESS_KEY_ID'],
    secret_access_key: ENV['AWS_SECRET_ACCESS_KEY']
  }
)
