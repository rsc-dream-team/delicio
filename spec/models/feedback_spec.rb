# frozen_string_literal: true

# == Schema Information
#
# Table name: feedbacks
#
#  id         :bigint           not null, primary key
#  email      :string
#  message    :string
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

require 'rails_helper'

RSpec.describe Feedback, type: :model do
  let!(:feedback) { FactoryBot.create(:feedback) }
  let!(:attributes) { %i[name email message] }

  it 'has attributes' do
    expect(feedback).to respond_to(*attributes)
  end

  it 'has a valid factory' do
    expect(feedback).to be_valid
  end

  describe 'validations' do
    it { should validate_presence_of(:name) }
    it { should validate_presence_of(:email) }
    it { should validate_presence_of(:message) }
  end
end
