# frozen_string_literal: true

# == Schema Information
#
# Table name: restrictions
#
#  id                :bigint           not null, primary key
#  restrictable_type :string
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  product_id        :bigint
#  restrictable_id   :bigint
#
# Indexes
#
#  index_restrictions_on_product_id       (product_id)
#  index_restrictions_on_restrictionable  (restrictable_type,restrictable_id)
#
# Foreign Keys
#
#  fk_rails_...  (product_id => products.id)
#

require 'rails_helper'

RSpec.describe Restriction, type: :model do
  it { should belong_to(:product) }
  it { should belong_to(:restrictable) }
end
