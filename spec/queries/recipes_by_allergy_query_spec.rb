# frozen_string_literal: true

require 'rails_helper'

describe RecipesByAllergyQuery do
  subject { described_class.new(allergy.id).call }
  let!(:allergy) { FactoryBot.create :allergy_with_restrictions }
  let!(:allowed_recipe) { FactoryBot.create(:ingredient_from_recipe).recipe }
  let!(:restricted_recipe) do
    FactoryBot.create(
      :ingredient_from_recipe,
      product_id: allergy.restrictions.first.product_id
    ).recipe
  end

  it 'display useful recipes' do
    expect(subject).to include allowed_recipe
    expect(subject).not_to include restricted_recipe
  end
end
