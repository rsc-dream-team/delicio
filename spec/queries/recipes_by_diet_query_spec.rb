# frozen_string_literal: true

require 'rails_helper'

describe RecipesByDietQuery do
  subject { described_class.new(diet.id).call }
  let!(:diet) { FactoryBot.create :diet_with_restrictions }
  let!(:allowed_recipe) { FactoryBot.create(:ingredient_from_recipe).recipe }
  let!(:restricted_recipe) do
    FactoryBot.create(
      :ingredient_from_recipe,
      product_id: diet.restrictions.first.product_id
    ).recipe
  end

  it 'display useful recipes' do
    expect(subject).to include allowed_recipe
    expect(subject).not_to include restricted_recipe
  end
end
