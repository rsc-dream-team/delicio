# frozen_string_literal: true

require 'rails_helper'

describe TopicPolicy do
  subject { described_class }
  let(:client) { FactoryBot.create(:client) }
  let(:nutritionist) { FactoryBot.create(:nutritionist) }
  let(:admin) { FactoryBot.create(:administrator) }

  let!(:topic) { FactoryBot.create(:topic) }

  permissions :new?, :update?, :edit?, :destroy? do
    it 'denies access if user is not an admin' do
      expect(subject).not_to permit(client, topic)
      expect(subject).not_to permit(nutritionist, topic)
    end

    it 'grants access if user is admin' do
      expect(subject).to permit(admin, topic)
    end

    it 'grants access if user is admin' do
      expect(subject).to permit(admin, topic)
    end
  end

  permissions :show? do
    it 'grants access to all users' do
      expect(subject).to permit(client, topic)
      expect(subject).to permit(nutritionist, topic)
    end
  end
end
