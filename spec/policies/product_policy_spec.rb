# frozen_string_literal: true

require 'rails_helper'

describe ProductPolicy do
  subject { described_class }
  let!(:client) { FactoryBot.create(:client) }
  let!(:nutritionist) { FactoryBot.create(:nutritionist) }
  let!(:admin) { FactoryBot.create(:administrator) }

  let!(:product) { FactoryBot.create(:product) }
  let!(:new_product) { FactoryBot.build(:product) }

  permissions :show?, :update?, :edit?, :destroy? do
    it 'denies access if user is not an admin' do
      expect(subject).not_to permit(client, product)
      expect(subject).not_to permit(nutritionist, product)
    end

    it 'grants access if user is admin' do
      expect(subject).to permit(admin, product)
    end
  end

  permissions :new?, :create? do
    it 'denies access if user is not an admin' do
      expect(subject).not_to permit(client, new_product)
      expect(subject).not_to permit(nutritionist, new_product)
    end

    it 'grants access if user is admin' do
      expect(subject).to permit(admin, new_product)
    end
  end
end
