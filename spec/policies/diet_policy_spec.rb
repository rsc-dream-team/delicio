# frozen_string_literal: true

require 'rails_helper'

describe DietPolicy do
  subject { described_class }
  let!(:client) { FactoryBot.create(:client) }
  let!(:nutritionist) { FactoryBot.create(:nutritionist) }

  let!(:owner) { diet.author }
  let!(:not_owner) { FactoryBot.create(:nutritionist) }

  let!(:diet) { FactoryBot.create(:diet) }
  let!(:new_diet) { FactoryBot.build(:diet) }

  permissions :update?, :destroy? do
    it 'denies access if user is not the owner of diet' do
      expect(subject).not_to permit(client, diet)
      expect(subject).not_to permit(not_owner, diet)
    end

    it 'grants access if user is the owner of diet' do
      expect(subject).to permit(owner, diet)
    end
  end

  permissions :new?, :create? do
    it 'denies access if user is not nutritionist' do
      expect(subject).not_to permit(client, new_diet)
    end

    it 'grants access if user is nutritionist' do
      expect(subject).to permit(nutritionist, new_diet)
    end
  end

  permissions :show? do
    it 'grants access to all users' do
      expect(subject).to permit(nutritionist, diet)
      expect(subject).to permit(client, diet)
    end
  end
end
