# frozen_string_literal: true

require 'rails_helper'

describe ArticlePolicy do
  subject { described_class }
  let!(:client) { FactoryBot.create(:client) }
  let!(:admin) { FactoryBot.create(:administrator) }
  let!(:nutritionist) { FactoryBot.create(:nutritionist) }

  let!(:article) { FactoryBot.create(:article) }

  let!(:new_article) { FactoryBot.build(:article) }
  let(:owner) { article.author }

  permissions :update?, :edit?, :destroy? do
    it 'denies access if user is not nutritionist or admin' do
      expect(subject).not_to permit(client, article)
    end

    it 'denies access if user is not the owner of article' do
      expect(subject).not_to permit(nutritionist, article)
    end

    it 'grants access if user the owner of article' do
      expect(subject).to permit(owner, article)
    end

    it 'grants access if user is admin' do
      expect(subject).to permit(admin, article)
    end
  end

  permissions :new? do
    it 'denies access if user is not nutritionist or admin' do
      expect(subject).not_to permit(client, new_article)
    end

    it 'grants access if user is admin' do
      expect(subject).to permit(admin, new_article)
    end

    it 'grants access if user is nutritionist' do
      expect(subject).to permit(nutritionist, new_article)
    end
  end

  permissions :show? do
    it 'grants access to all users' do
      expect(subject).to permit(client, article)
      expect(subject).to permit(nutritionist, article)
    end
  end
end
