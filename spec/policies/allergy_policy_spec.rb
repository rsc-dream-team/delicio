# frozen_string_literal: true

require 'rails_helper'

describe AllergyPolicy do
  subject { described_class }
  let!(:client) { FactoryBot.create(:client) }
  let!(:nutritionist) { FactoryBot.create(:nutritionist) }
  let!(:admin) { FactoryBot.create(:administrator) }

  let!(:allergy) { FactoryBot.create(:allergy) }
  let!(:new_allergy) { FactoryBot.build(:allergy) }

  permissions :update?, :edit?, :destroy? do
    it 'denies access if user is not an admin' do
      expect(subject).not_to permit(client, allergy)
      expect(subject).not_to permit(nutritionist, allergy)
    end

    it 'grants access if user is admin' do
      expect(subject).to permit(admin, allergy)
    end
  end

  permissions :new?, :create? do
    it 'denies access if user is not an admin' do
      expect(subject).not_to permit(client, new_allergy)
      expect(subject).not_to permit(nutritionist, new_allergy)
    end

    it 'grants access if user is admin' do
      expect(subject).to permit(admin, new_allergy)
    end
  end

  permissions :show?, :index? do
    it 'grants access to all users' do
      expect(subject).to permit(client, allergy)
      expect(subject).to permit(nutritionist, allergy)
    end
  end
end
