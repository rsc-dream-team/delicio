# frozen_string_literal: true

require 'rails_helper'

describe RecipePolicy do
  subject { described_class }

  let(:client)       { FactoryBot.create(:client) }
  let(:nutritionist) { FactoryBot.create(:nutritionist) }
  let(:admin)        { FactoryBot.create(:administrator) }

  let!(:recipe)      { FactoryBot.create(:recipe) }

  permissions :new?, :update?, :edit?, :destroy?, :create? do
    it 'denies access if user is not an admin' do
      expect(subject).not_to permit(client, recipe)
      expect(subject).not_to permit(nutritionist, recipe)
    end

    it 'grants access if user is admin' do
      expect(subject).to permit(admin, recipe)
    end
  end

  permissions :show?, :index? do
    it 'grants access if user is client' do
      expect(subject).to permit(client, recipe)
    end

    it 'grants access if user is admin' do
      expect(subject).to permit(admin, recipe)
    end

    it 'grants access if user is nutritionist' do
      expect(subject).to permit(nutritionist, recipe)
    end
  end
end
