# frozen_string_literal: true

FactoryBot.define do
  factory :recipe do
    name              { Faker::DcComics.hero }
    description       { Faker::Book.genre }
    preparation_kind  { 'frying' }
    preparation_time  { 1 }
  end
end
