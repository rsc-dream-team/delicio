# frozen_string_literal: true

# == Schema Information
#
# Table name: nutritionist_profiles
#
#  id               :bigint           not null, primary key
#  birthday         :date
#  experience_years :integer
#  first_name       :string
#  how_heard        :string
#  last_name        :string
#  work             :string
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  user_id          :bigint
#
# Indexes
#
#  index_nutritionist_profiles_on_user_id  (user_id)
#
# Foreign Keys
#
#  fk_rails_...  (user_id => users.id)
#

FactoryBot.define do
  factory :nutritionist_profile do
    first_name       { Faker::DcComics.hero }
    last_name        { Faker::DcComics.hero }
    work             { Faker::DcComics.hero }
    birthday         { Faker::Date.birthday(18, 65) }
    experience_years { Faker::Number.non_zero_digit }
    how_heard        { Faker::DcComics.hero }
    nutritionist     { user_id }
  end
end
