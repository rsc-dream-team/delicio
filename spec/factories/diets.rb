# frozen_string_literal: true

# == Schema Information
#
# Table name: diets
#
#  id                 :bigint           not null, primary key
#  description        :string
#  image_content_type :string
#  image_file_name    :string
#  image_file_size    :integer
#  image_updated_at   :datetime
#  name               :string
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  user_id            :bigint
#
# Indexes
#
#  index_diets_on_user_id  (user_id)
#
# Foreign Keys
#
#  fk_rails_...  (user_id => users.id)
#

FactoryBot.define do
  factory :diet do
    name        { Faker::DcComics.hero }
    description { Faker::Book.genre }

    association :author, factory: :nutritionist

    factory :diet_with_restrictions do
      after(:create) do |diet, _evaluator|
        create_list(:restriction, 4, restrictable: diet)
      end
    end
  end
end
