# frozen_string_literal: true

# == Schema Information
#
# Table name: recommendations
#
#  id                      :bigint           not null, primary key
#  recommendationable_type :string
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#  product_id              :bigint
#  recommendationable_id   :bigint
#
# Indexes
#
#  index_recommendations_on_product_id          (product_id)
#  index_recommendations_on_recommendationable  (recommendationable_type,recommendationable_id)
#
# Foreign Keys
#
#  fk_rails_...  (product_id => products.id)
#

FactoryBot.define do
  factory :recommendation do
    association :recommendationable, factory: :diet
    association :product
  end
end
