# frozen_string_literal: true

FactoryBot.define do
  factory :product do
    name      { Faker::Food.ingredient }
    category  { Product.categories.keys.sample }
    kal       { Faker::Number.non_zero_digit }
  end
end
