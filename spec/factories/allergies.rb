# frozen_string_literal: true

# == Schema Information
#
# Table name: allergies
#
#  id                 :bigint           not null, primary key
#  description        :string
#  image_content_type :string
#  image_file_name    :string
#  image_file_size    :integer
#  image_updated_at   :datetime
#  name               :string
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#

FactoryBot.define do
  factory :allergy do
    name        { Faker::DcComics.hero }
    description { Faker::Book.genre }

    factory :allergy_with_restrictions do
      after(:create) do |allergy, _evaluator|
        create_list(:restriction, 4, restrictable: allergy)
      end
    end
  end
end
