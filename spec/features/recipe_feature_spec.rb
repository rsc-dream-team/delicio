# frozen_string_literal: true

require 'rails_helper'

feature 'Create, edit, delete recipe flow by Admin: ' do
  let!(:administrator) { FactoryBot.create(:administrator) }
  let!(:product)       { FactoryBot.create(:product) }
  let!(:recipe)        { FactoryBot.build(:recipe) }
  let!(:ingredient)    { FactoryBot.build(:ingredient) }

  before do
    visit user_session_path

    within('#new_user') do
      fill_in 'Email',    with: administrator.email
      fill_in 'Password', with: administrator.password
    end

    click_button 'Sign in'
  end

  context 'create recipe with valid params' do
    scenario 'adding new recipe without ingredients' do
      visit new_recipe_path

      fill_in 'Recipe name',          with: recipe.name
      fill_in 'Description',          with: recipe.description
      select recipe.preparation_kind, from: 'Preparation_kind'
      fill_in 'Preparation_time',     with: recipe.preparation_time

      click_button 'Create Recipe'

      expect(page).to have_content recipe.name
      expect(page).to have_content recipe.description
      expect(page).to have_content recipe.preparation_kind
      expect(page).to have_content recipe.preparation_time
    end

    xscenario 'adding new recipe with ingredients' do
      visit new_recipe_path

      within('form#new_recipe') do
        fill_in 'Recipe name',          with: recipe.name
        fill_in 'Description',          with: recipe.description
        select recipe.preparation_kind, from: 'Preparation_kind'
        fill_in 'Preparation_time',     with: recipe.preparation_time

        click_on 'Add ingredient'

        first("input[name='recipe[ingredients_attributes][0][weight]']").set ingredient.weight

        all('weights').each do |input|
          input.set(ingredient.weight)
        end

        all('products').each do |input|
          input.set(product)
        end

        click_button 'Create Recipe'
      end

      expect(page).to have_content ingredient.weight
      expect(page).to have_content recipe.name
      expect(page).to have_content recipe.description
      expect(page).to have_content recipe.preparation_kind
      expect(page).to have_content recipe.preparation_time
    end
  end

  context 'create recipe with invalid params' do
    scenario 'only adding a name to the recipe' do
      visit new_recipe_path

      fill_in 'Recipe name', with: recipe.name

      click_button 'Create Recipe'

      expect(page).to have_content "Preparation kind can't be blank"
      expect(page).to have_no_content recipe.name
    end
  end

  context 'modify existing recipe' do
    let!(:recipe_1) { FactoryBot.create(:recipe) }
    let!(:recipe_2) { FactoryBot.attributes_for(:recipe) }

    scenario 'edit the recipe' do
      visit recipes_path

      expect(page).to have_content recipe_1.name

      click_on recipe_1.name

      click_on 'Edit'

      fill_in 'Recipe name', with: recipe_2[:name]

      click_button 'Update Recipe'

      expect(page).to have_content 'Recipe updated'
      expect(page).to have_content recipe_2[:name]
    end

    scenario 'delete the recipe' do
      visit recipes_path

      click_on recipe_1.name

      click_on 'Edit'

      click_on 'Delete'

      expect(page).to have_content 'Recipe deleted'
      expect(page).to have_no_content recipe_1.name
    end
  end
end
