# frozen_string_literal: true

require 'rails_helper'

feature 'Create, edit, delete topic flow by Admin:' do
  let!(:administrator) { FactoryBot.create(:administrator) }
  let(:topic)          { FactoryBot.build(:topic) }
  let!(:topic_1)       { FactoryBot.create(:topic) }

  before do
    visit user_session_path

    fill_in 'Email',    with: administrator.email
    fill_in 'Password', with: administrator.password

    click_button 'Sign in'
  end

  context 'create topic with valid params' do
    scenario 'adding a new topic' do
      visit topics_path

      click_on 'Add a New topic'

      fill_in 'Title',       with: topic.name
      fill_in 'Description', with: topic.description

      click_on 'Create Topic'

      visit topics_path
      expect(page).to have_content topic.name
    end
  end

  context 'create topic with invalid params' do
    scenario 'adding only an topic name' do
      visit new_topic_path

      fill_in 'Title', with: topic.name

      click_on 'Create Topic'

      expect(page).to have_content "Description can't be blank"
      expect(page).to have_no_content topic.name
    end

    scenario 'adding only an topic description' do
      visit new_topic_path

      fill_in 'Description', with: topic.description

      click_on 'Create Topic'

      expect(page).to have_content "Name can't be blank"
      expect(page).to have_no_content topic.name
    end
  end

  context 'modify existing topic' do
    let!(:topic_2) { FactoryBot.attributes_for(:topic) }

    scenario 'edit existing topic' do
      visit topics_path

      click_on topic_1.name

      click_on 'Edit'
      fill_in 'Title', with: topic_2[:name]
      click_on 'Update Topic'

      expect(page).to have_content 'Updated topic'
      expect(page).to have_content topic_2[:name]
    end

    scenario 'remove topic' do
      visit topics_path

      click_on topic_1.name

      click_on 'Delete'

      expect(page).to have_content 'Deleted topic'
      expect(page).to have_no_content topic_1.name
    end
  end
end
