# frozen_string_literal: true

require 'rails_helper'

feature 'Create, edit, delete product flow by Admin: ' do
  let!(:administrator)      { FactoryBot.create(:administrator) }
  let!(:product)            { FactoryBot.build(:product) }

  before do
    visit user_session_path

    within('#new_user') do
      fill_in 'Email',    with: administrator.email
      fill_in 'Password', with: administrator.password
    end

    click_button 'Sign in'
  end

  context 'create product with valid params' do
    scenario 'adding a new product' do
      visit new_product_path

      within('form') do
        fill_in 'Product name',  with: product.name
        select product.category, from: 'Category of product'
        fill_in 'Calories',      with: product.kal

        click_button 'Add product'
      end

      expect(page).to have_content product.name
      expect(page).to have_content product.category
      expect(page).to have_content product.kal
    end
  end

  context 'create product with invalid params' do
    scenario 'adding only a product name' do
      visit new_product_path

      within('form') do
        fill_in 'Product name', with: product.name

        click_button 'Add product'
      end

      expect(page).to have_content "Category can't be blank and Kal can't be blank"

      visit products_path
      expect(page).to have_no_content product.name
    end
  end

  context 'modify existing product' do
    let!(:product_1) { FactoryBot.create(:product) }
    let!(:product_2) { FactoryBot.attributes_for(:product) }

    scenario 'edit the product' do
      visit products_path

      expect(page).to have_content product_1.name

      click_on product_1.name

      click_on 'Edit'

      within('form') do
        fill_in 'Product name', with: product_2[:name]

        click_button 'Submit'
      end

      expect(page).to have_content product_2[:name]
    end

    scenario 'delete the product' do
      visit products_path

      click_on product_1.name

      click_on 'Remove'

      expect(page).to have_no_content product_1.name
    end
  end
end
