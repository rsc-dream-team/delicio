# frozen_string_literal: true

require 'rails_helper'

feature 'Create, edit, delete diet flow by Nutritionist:' do
  context 'new diet' do
    let!(:diet)      { FactoryBot.create :diet }
    let!(:diet_1)    { FactoryBot.build :diet }
    let!(:diet_name) { diet.name }
    let!(:user)      { diet.author }

    before do
      visit user_session_path

      fill_in 'Email',    with: user.email
      fill_in 'Password', with: user.password

      click_button 'Sign in'
    end

    context 'create diet with valid params' do
      scenario 'adding a new diet' do
        visit new_diet_path

        fill_in 'diet[name]',  with: diet_1.name
        fill_in 'Description', with: diet_1.description

        click_on 'Create Diet'

        visit diets_path

        expect(page).to have_content diet_1.name
      end
    end

    context 'create diet with invalid params' do
      let!(:diet_2)      { FactoryBot.build :diet }

      scenario 'adding only a name of a diet' do
        visit new_diet_path

        fill_in 'diet[name]', with: diet_2.name

        click_on 'Create Diet'
        expect(page).to have_content "Description can't be blank"

        visit diets_path
        expect(page).to have_no_content diet_2.name
      end
    end

    context 'modify existing diet' do
      let!(:diet_4) { FactoryBot.attributes_for(:diet) }

      scenario 'edit the diet' do
        visit diets_path

        click_on diet.name

        click_on 'Edit'

        within('form') do
          fill_in 'diet[name]', with: diet_4[:name]

          click_button 'Update Diet'
        end

        expect(page).to have_content diet_4[:name]
      end
    end

    scenario 'remove diet' do
      visit diet_path(diet)

      click_on 'Remove'

      expect(page).to have_no_content diet_name
    end
  end

  context 'display useful recipes' do
    let!(:client)  { FactoryBot.create(:client) }
    let!(:diet) { FactoryBot.create :diet_with_restrictions }
    let!(:allowed_ingredient) { FactoryBot.create(:ingredient_from_recipe) }
    let!(:restricted_ingredient) do
      FactoryBot.create(
        :ingredient_from_recipe,
        product_id: diet.restrictions.first.product_id
      )
    end

    before do
      visit user_session_path

      fill_in 'Email',    with: client.email
      fill_in 'Password', with: client.password

      click_button 'Sign in'
    end

    scenario 'display only allowed recipes' do
      visit diet_path(diet.id)

      expect(page).to have_content allowed_ingredient.recipe.name
      expect(page).not_to have_content restricted_ingredient.recipe.name
    end
  end
end
