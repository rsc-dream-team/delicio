# frozen_string_literal: true

require 'rails_helper'

feature 'User editing feature' do
  let!(:administrator) { FactoryBot.create(:administrator) }
  let!(:client) { FactoryBot.create(:client) }
  let!(:nutritionist) { FactoryBot.create(:nutritionist) }
  let!(:first_name) { Faker::Name.first_name }
  let!(:last_name) { Faker::Name.last_name }
  let(:fall_back_url) { root_path(locale: I18n.locale) }

  context 'Client edits profile' do
    before do
      visit new_user_session_path

      within('#new_user') do
        fill_in 'Email', with: client.email
        fill_in 'Password', with: client.password
      end

      click_button 'Sign in'
    end

    it 'Updates profile' do
      visit edit_user_profiles_path
      fill_in 'First name', with: first_name
      fill_in 'Last name', with: last_name

      click_button 'Update'

      expect(page).to have_current_path(fall_back_url)
    end
  end

  context 'Administrator edits profile' do
    before do
      visit new_user_session_path

      within('#new_user') do
        fill_in 'Email', with: administrator.email
        fill_in 'Password', with: administrator.password
      end

      click_button 'Sign in'
    end

    it 'Updates profile' do
      visit edit_user_profiles_path
      fill_in 'First name', with: first_name
      fill_in 'Last name', with: last_name

      click_button 'Update'

      expect(page).to have_current_path(fall_back_url)
    end
  end

  context 'Nutritionist edits profile' do
    before do
      visit new_user_session_path

      within('#new_user') do
        fill_in 'Email', with: nutritionist.email
        fill_in 'Password', with: nutritionist.password
      end

      click_button 'Sign in'
    end

    it 'Updates profile' do
      visit edit_user_profiles_path
      fill_in 'First name', with: first_name
      fill_in 'Last name', with: last_name

      click_button 'Update'

      expect(page).to have_current_path(fall_back_url)
    end
  end
end
