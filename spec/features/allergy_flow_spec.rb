# frozen_string_literal: true

require 'rails_helper'

feature 'Create, edit, delete allergy flow by Admin:' do
  context 'new allergy' do
    let!(:administrator) { FactoryBot.create(:administrator) }
    let(:allergy)        { FactoryBot.build(:allergy) }

    before do
      visit user_session_path

      fill_in 'Email',    with: administrator.email
      fill_in 'Password', with: administrator.password

      click_button 'Sign in'
    end

    context 'create allergy with valid params' do
      scenario 'adding a new allergy' do
        visit allergies_path

        click_on 'Add a new allergy'

        fill_in 'Name',        with: allergy.name
        fill_in 'Description', with: allergy.description

        click_on 'Create Allergy'

        expect(page).to have_content allergy.name
      end
    end

    context 'create allergy with invalid params' do
      scenario 'adding only an allergy name' do
        visit new_allergy_path

        fill_in 'Name', with: allergy.name

        click_on 'Create Allergy'

        expect(page).to have_content "Description can't be blank"
        expect(page).to have_no_content allergy.name
      end

      scenario 'adding only an allergy description' do
        visit new_allergy_path

        fill_in 'Description', with: allergy.description

        click_on 'Create Allergy'

        expect(page).to have_content "Name can't be blank"
        expect(page).to have_no_content allergy.name
      end
    end

    context 'modify existing allergy' do
      let!(:allergy_1) { FactoryBot.create(:allergy) }
      let!(:allergy_2) { FactoryBot.attributes_for(:allergy) }

      scenario 'edit existing allergy' do
        visit allergies_path

        click_on allergy_1.name

        click_on 'Edit'
        fill_in 'Name', with: allergy_2[:name]
        click_on 'Update Allergy'

        expect(page).to have_content 'Updated allergy'
        expect(page).to have_content allergy_2[:name]
      end

      scenario 'remove allergy' do
        visit allergies_path

        click_on allergy_1.name

        click_on 'Remove'

        expect(page).to have_content 'Deleted allergy'
        expect(page).to have_no_content allergy_1.name
      end
    end
  end

  context 'display useful recipes' do
    let!(:client)  { FactoryBot.create(:client) }
    let!(:allergy) { FactoryBot.create :allergy_with_restrictions }
    let!(:allowed_ingredient) { FactoryBot.create(:ingredient_from_recipe) }
    let!(:restricted_ingredient) do
      FactoryBot.create(
        :ingredient_from_recipe,
        product_id: allergy.restrictions.first.product_id
      )
    end

    before do
      visit user_session_path

      fill_in 'Email',    with: client.email
      fill_in 'Password', with: client.password

      click_button 'Sign in'
    end

    scenario 'display only allowed recipes' do
      visit allergy_path(allergy.id)

      expect(page).to have_content allowed_ingredient.recipe.name
      expect(page).not_to have_content restricted_ingredient.recipe.name
    end
  end
end
