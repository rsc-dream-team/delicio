# frozen_string_literal: true

require 'rails_helper'

feature 'Create, edit, delete article flow by Nutritionist: ' do
  let!(:nutritionist) { FactoryBot.create(:nutritionist) }
  let!(:topic)        { FactoryBot.create(:topic) }
  let!(:article)      { FactoryBot.build(:article, user_id: nutritionist.id, topic_id: topic.id) }
  let!(:article_2)    { FactoryBot.attributes_for(:article) }

  before do
    visit user_session_path

    within('#new_user') do
      fill_in 'Email',    with: nutritionist.email
      fill_in 'Password', with: nutritionist.password
    end

    click_button 'Sign in'
  end

  xcontext 'create article with valid params' do
    scenario 'adding a new article' do
      visit new_article_path

      select topic.name
      fill_in 'Title', with: article.title
      # fill_in 'Content', with: article.content, visible: false
      # find('trix-editor').click.set('Some content')
      # element = page.find('trix-editor')
      # element.set(article.content)

      click_button 'Create Article'

      visit articles_path
      expect(page).to have_content article.title
    end
  end

  context 'create article with invalid params' do
    let!(:article_1) { FactoryBot.build(:article, user_id: nutritionist.id, topic_id: topic.id) }

    scenario 'adding only an article title' do
      visit new_article_path

      fill_in 'Title', with: article_1.title

      click_button 'Create Article'

      expect(page).to have_content "Content can't be blank"

      visit articles_path
      expect(page).to have_no_content article_1.title
    end
  end

  context 'modify existing article' do
    let!(:article) { FactoryBot.create(:article, user_id: nutritionist.id, topic_id: topic.id) }

    scenario 'Nutritionist edits an article' do
      visit article_path(article)

      click_on 'Edit'

      fill_in 'Title', with: article_2[:title]

      click_button 'Update Article'

      visit articles_path
      expect(page).to have_content article_2[:title]
    end

    scenario 'Nutritionist deletes an article' do
      visit article_path(article)

      expect(page).to have_content article.title

      click_on 'Delete'

      visit articles_path

      expect(page).to have_no_content article.title
    end
  end
end
