# frozen_string_literal: true

require 'rails_helper'

feature 'Client registration:' do
  let!(:client)        { FactoryBot.build(:client) }
  let!(:client_1)      { FactoryBot.create(:client) }
  let!(:client_2)      { FactoryBot.build(:client) }
  let!(:fake_email)    { 'hello@' }
  let!(:fake_password) { '1234' }
  let(:fall_back_url) { root_path(locale: I18n.locale) }

  context 'with valid params' do
    scenario 'allows user to sign up' do
      visit new_user_registration_path(role: :client)

      fill_in 'Email',    with: client.email
      fill_in 'Password', with: client.password

      click_button 'Sign up'

      expect(page).to have_current_path(fall_back_url)
    end
  end

  context 'with invalid params' do
    scenario 'without password' do
      visit new_user_registration_path(role: :client)

      fill_in 'Email', with: client_2.email

      click_button 'Sign up'

      expect(page).to have_content "Password can't be blank"
    end

    scenario 'without email' do
      visit new_user_registration_path(role: :client)

      fill_in 'Password', with: client_2.password

      click_button 'Sign up'

      expect(page).to have_content "Email can't be blank"
    end

    scenario 'with email that is already used' do
      visit new_user_registration_path(role: :client)

      fill_in 'Email', with: client_1.email

      click_button 'Sign up'

      expect(page).to have_content 'Email has already been taken'
    end

    scenario 'with existing email in different letters case' do
      visit new_user_registration_path(role: :client)

      fill_in 'Email', with: client_1.email.upcase

      click_button 'Sign up'

      expect(page).to have_content 'Email has already been taken'
    end
  end

  context 'with invalid email' do
    scenario 'it shows errors' do
      visit new_user_registration_path(role: :client)

      fill_in 'Email', with: fake_email

      click_button 'Sign up'

      expect(page).to have_content 'Email is invalid'
    end
  end

  context 'when password is too short' do
    scenario 'it shows errors' do
      visit new_user_registration_path(role: :client)

      fill_in 'Password', with: fake_password

      click_button 'Sign up'

      expect(page).to have_content 'Password is too short (minimum is 6 characters)'
    end
  end
end
