# frozen_string_literal: true

require 'rails_helper'

feature 'Nutritionist registration:' do
  let!(:nutritionist)   { FactoryBot.build(:nutritionist) }
  let!(:nutritionist_1) { FactoryBot.create(:nutritionist) }
  let!(:nutritionist_2) { FactoryBot.build(:nutritionist) }
  let!(:fake_email)     { 'hello@' }
  let!(:fake_password)  { '1234' }
  let(:fall_back_url) { root_path(locale: I18n.locale) }

  context 'with valid params' do
    scenario 'allows nutritionist to sign up' do
      visit new_user_registration_path(role: :nutritionist)

      fill_in 'Email',    with: nutritionist.email
      fill_in 'Password', with: nutritionist.password
      fill_in 'nutritionist_profile_attributes_experience_years',
              with: Faker::Number.non_zero_digit

      click_button 'Sign up'

      expect(page).to have_current_path(fall_back_url)
    end
  end

  context 'with invalid params' do
    scenario 'without password' do
      visit new_user_registration_path(role: :nutritionist)

      fill_in 'Email', with: nutritionist_2.email

      click_button 'Sign up'

      expect(page).to have_content "Password can't be blank"
    end

    scenario 'without email' do
      visit new_user_registration_path(role: :nutritionist)

      fill_in 'Password', with: nutritionist_2.password

      click_button 'Sign up'

      expect(page).to have_content "Email can't be blank"
    end

    scenario 'with email that is already used' do
      visit new_user_registration_path(role: :nutritionist)

      fill_in 'Email', with: nutritionist_1.email

      click_button 'Sign up'

      expect(page).to have_content 'Email has already been taken'
    end

    scenario 'with existing email in different letters case' do
      visit new_user_registration_path(role: :nutritionist)

      fill_in 'Email', with: nutritionist_1.email.upcase

      click_button 'Sign up'

      expect(page).to have_content 'Email has already been taken'
    end
  end

  context 'with invalid email' do
    scenario 'it shows errors' do
      visit new_user_registration_path(role: :nutritionist)

      fill_in 'Email', with: fake_email

      click_button 'Sign up'

      expect(page).to have_content 'Email is invalid'
    end
  end

  context 'when password is too short' do
    scenario 'it shows errors' do
      visit new_user_registration_path(role: :nutritionist)

      fill_in 'Password', with: fake_password

      click_button 'Sign up'

      expect(page).to have_content 'Password is too short (minimum is 6 characters)'
    end
  end
end
