# frozen_string_literal: true

require 'rails_helper'

feature 'creating a new feedback' do
  let!(:feedback) { FactoryBot.create(:feedback) }

  scenario 'adding a new feedback' do
    visit about_us_path

    fill_in 'feedback_name',    with: feedback.name
    fill_in 'feedback_email',   with: feedback.email
    fill_in 'feedback_message', with: feedback.message

    click_on 'Send'

    expect(page).to have_content 'We have received your message and will be in touch soon!'
  end
end
