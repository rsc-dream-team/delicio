class CreateRecommendations < ActiveRecord::Migration[5.2]
  def change
    create_table :recommendations do |t|
      t.references :product, foreign_key: true
      t.references :recommendationable, polymorphic: true,
        index: { name: :index_recommendations_on_recommendationable }

      t.timestamps
    end
  end
end
