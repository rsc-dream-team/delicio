class AddImageToDiets < ActiveRecord::Migration[5.2]
  def change
    add_column :diets, :image, :string
  end
end
