class AddImageToAllergies < ActiveRecord::Migration[5.2]
  def change
    add_column :allergies, :image, :string
  end
end
