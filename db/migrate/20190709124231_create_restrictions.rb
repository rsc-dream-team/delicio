class CreateRestrictions < ActiveRecord::Migration[5.2]
  def change
    create_table :restrictions do |t|
      t.references :product, foreign_key: true
      t.references :restrictable, polymorphic: true,
        index: { name: :index_restrictions_on_restrictionable }

      t.timestamps
    end
  end
end
