class ReplaceCarrierwaveWithPaperclip < ActiveRecord::Migration[5.2]
  def change
    remove_column :allergies, :image
    remove_column :diets,     :image
    remove_column :products,  :image
    remove_column :recipes,   :image
    remove_column :articles,  :image

    add_attachment :allergies, :image
    add_attachment :diets,     :image
    add_attachment :products,  :image
    add_attachment :recipes,   :image
    add_attachment :articles,  :image
  end
end
