class AddUserToDiets < ActiveRecord::Migration[5.2]
  def change
    add_reference :diets, :user, foreign_key: true
  end
end
