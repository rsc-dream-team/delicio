# frozen_string_literal: true

class FeedbacksMailer < ApplicationMailer
  def general_message(feedback)
    @feedback = feedback

    mail(
      to:      @feedback.email,
      subject: 'Thanks for your feedback!'
    )
  end
end
