# frozen_string_literal: true

class ApplicationMailer < ActionMailer::Base
  DEFAUL_FROM_EMAIL = 'support@delicio.com'

  default from: DEFAUL_FROM_EMAIL
  layout 'mailer'
end
