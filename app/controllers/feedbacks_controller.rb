# frozen_string_literal: true

class FeedbacksController < ApplicationController
  skip_before_action :authenticate_user!

  def create
    @feedback = Feedback.new(feedback_params)

    if @feedback.save
      sending_email
      redirect_to about_us_path, notice: 'We have received your message and will be in touch soon!'
    else
      redirect_to about_us_path, notice: 'There was an error sending your message! Try again.'
    end
  end

  private

  def feedback_params
    params.require(:feedback).permit(:name, :email, :message)
  end

  def sending_email
    FeedbacksMailer.general_message(@feedback).deliver
  end
end
