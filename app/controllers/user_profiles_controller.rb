# frozen_string_literal: true

class UserProfilesController < ApplicationController
  before_action :load_profile, only: %i[show edit update destroy]

  def edit; end

  def update
    @profile.update(profile_params)
    redirect_to root_path, flash: { notice: 'Profile was updated' }
  end

  private

  def load_profile
    @profile = current_user.profile || current_user.build_profile
  end

  def user_profile
    "#{current_user.type.downcase}_profile".to_sym
  end

  def profile_params
    params.require(user_profile).permit(:name, :description,
                                        :first_name, :last_name, :nickname,
                                        :work, :how_heard, :birthday, :experience_years)
  end
end
