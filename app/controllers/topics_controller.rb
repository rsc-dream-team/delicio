# frozen_string_literal: true

class TopicsController < ApplicationController
  before_action :authenticate_user!, except: :index
  before_action :current_topic, only: %i[show edit update destroy]
  before_action :authorize!

  def index
    @topics = Topic.all
  end

  def new
    @topic = Topic.new
  end

  def create
    @topic = Topic.new(topic_params)

    if @topic.save
      redirect_to topic_path(@topic),
                  flash: { notice: 'Topic was successfully created.' }
    else
      redirect_to new_topic_path,
                  flash: { error: @topic.errors.full_messages.to_sentence }
    end
  end

  def show; end

  def edit; end

  def update
    if @topic.update(topic_params)
      redirect_to topic_path(@topic),
                  flash: { notice: 'Updated topic' }
    else
      render :edit,
             flash: { error: @topic.errors.full_messages.to_sentence }
    end
  end

  def destroy
    if @topic.destroy
      redirect_to topics_path,
                  flash: { notice: 'Deleted topic' }
    else
      redirect_to topic_path(@topic),
                  flash: { error: @topic.errors.full_messages.to_sentence }
    end
  end

  private

  def topic_params
    params.require(:topic).permit(:name, :description, :user_id)
  end

  def current_topic
    @topic = Topic.find(params[:id])
  end

  def authorize!
    authorize(@topic || Topic)
  end
end
