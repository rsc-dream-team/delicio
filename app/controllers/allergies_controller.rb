# frozen_string_literal: true

class AllergiesController < ApplicationController
  before_action :authenticate_user!, except: :index
  before_action :current_allergy, only: %i[show edit update destroy]
  before_action :authorize!

  def index
    @allergies = Allergy.all
  end

  def show
    @recipes = RecipesByAllergyQuery.new(params[:id]).call
  end

  def new
    @allergy = Allergy.new
  end

  def create
    @allergy = Allergy.new(allergy_params)

    if @allergy.save
      redirect_to allergy_path(@allergy),
                  flash: { notice: 'Allergy was successfully created.' }
    else
      redirect_to allergies_path,
                  flash: { error: @allergy.errors.full_messages.to_sentence }
    end
  end

  def edit; end

  def update
    if @allergy.update(allergy_params)
      redirect_to allergy_path(@allergy),
                  flash: { notice: 'Updated allergy.' }
    else
      render :edit,
             flash: { error: @allergy.errors.full_messages.to_sentence }
    end
  end

  def destroy
    if @allergy.destroy
      redirect_to allergies_path,
                  flash: { notice: 'Deleted allergy.' }
    else
      render :edit,
             flash: { error: @allergy.errors.full_messages.to_sentence }
    end
  end

  private

  def allergy_params
    params.require(:allergy).permit!
  end

  def current_allergy
    @allergy = Allergy.find(params[:id])
  end

  def authorize!
    authorize(@allergy || Allergy)
  end
end
