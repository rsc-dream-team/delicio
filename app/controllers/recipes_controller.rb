# frozen_string_literal: true

class RecipesController < ApplicationController
  before_action :authenticate_user!, except: :index
  before_action :recipe, only: %i[show update destroy edit]
  before_action :authorize!

  def index
    @recipes = Recipe.all
  end

  def new
    @recipe = Recipe.new
  end

  def show; end

  def create
    @recipe = Recipe.new(recipe_params)

    if @recipe.save
      redirect_to recipe_path(@recipe),
                  flash: { notice: 'Recipe was successfully created' }
    else
      redirect_to new_recipe_path,
                  flash: { error: @recipe.errors.full_messages.to_sentence }
    end
  end

  def update
    if @recipe.update_attributes(recipe_params)
      redirect_to recipe_path(@recipe),
                  flash: { notice: 'Recipe updated' }
    else
      redirect_to edit_recipe_path(@recipe),
                  flash: { error: @recipe.errors.full_messages.to_sentence }
    end
  end

  def edit; end

  def destroy
    @recipe.destroy
    redirect_to recipes_path, flash: { notice: 'Recipe deleted' }
  end

  private

  def recipe_params
    params.require(:recipe).permit!
  end

  def recipe
    @recipe = Recipe.find(params[:id])
  end

  def authorize!
    authorize(@recipe || Recipe)
  end
end
