# frozen_string_literal: true

class DietsController < ApplicationController
  before_action :authenticate_user!, except: :index
  before_action :current_diet, only: %i[show edit update destroy]
  before_action :authorize!

  def index
    @diets = Diet.all
  end

  def show
    @recipes = RecipesByDietQuery.new(params[:id]).call
  end

  def new
    @diet = Diet.new
  end

  def create
    @diet = Diet.new(diet_params)
    if @diet.save
      redirect_to diets_path,
                  flash: { notice: 'Diet was successfully created.' }
    else
      redirect_to new_diet_path,
                  flash: { error: @diet.errors.full_messages.to_sentence }
    end
  end

  def edit; end

  def update
    if @diet.update_attributes(diet_params)
      redirect_to diet_path(@diet),
                  flash: { notice: 'Diet updated.' }
    else
      render :edit,
             flash: { error: @diet.errors.full_messages.to_sentence }
    end
  end

  def destroy
    if @diet.destroy
      redirect_to diets_path,
                  flash: { notice: 'Diet deleted.' }
    else
      redirect_to diet_path(@diet),
                  flash: { error: @diet.errors.full_messages.to_sentence }
    end
  end

  private

  def diet_params
    params.require(:diet).permit!
  end

  def current_diet
    @diet = Diet.find(params[:id])
  end

  def authorize!
    authorize(@diet || Diet)
  end
end
