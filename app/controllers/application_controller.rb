# frozen_string_literal: true

class ApplicationController < ActionController::Base
  include Pundit
  before_action :authenticate_user!
  before_action :set_locale

  rescue_from Pundit::NotAuthorizedError do
    redirect_to root_url, alert: 'You do not have access to this page'
  end

  private

  def set_locale
    I18n.locale = session[:locale] || I18n.default_locale
  end

  def default_url_options
    { locale: I18n.locale }
  end
end
