# frozen_string_literal: true

class AboutUsController < ApplicationController
  skip_before_action :authenticate_user!

  def index
    @feedback = Feedback.new
  end
end
