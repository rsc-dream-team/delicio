# frozen_string_literal: true

class DietPolicy < ApplicationPolicy
  def show?
    true
  end

  def new?
    nutritionist?
  end

  def edit?
    owner?
  end

  def destroy?
    owner?
  end

  def owner?
    @record.author == @user
  end

  alias_method :create?, :new?
  alias_method :update?, :edit?
  alias_method :index?,  :show?
end
