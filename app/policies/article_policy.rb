# frozen_string_literal: true

class ArticlePolicy < ApplicationPolicy
  def show?
    true
  end

  def new?
    nutritionist? || admin?
  end

  def edit?
    owner? || admin?
  end

  def destroy?
    owner? || admin?
  end

  def owner?
    @record.author == @user
  end

  alias_method :create?, :new?
  alias_method :update?, :edit?
  alias_method :index?,  :show?
end
