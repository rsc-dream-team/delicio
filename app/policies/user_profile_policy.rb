# frozen_string_literal: true

class UserProfilePolicy < ApplicationPolicy
  def edit?
    owner?
  end

  private

  def owner?
    record.user_id == user.id
  end

  alias_method :update?, :edit?
end
