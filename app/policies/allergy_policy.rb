# frozen_string_literal: true

class AllergyPolicy < ApplicationPolicy
  def show?
    true
  end

  def edit?
    admin?
  end

  def new?
    admin?
  end

  def destroy?
    admin?
  end

  alias_method :create?, :new?
  alias_method :update?, :edit?
  alias_method :index?,  :show?
end
