# frozen_string_literal: true

class ProductPolicy < ApplicationPolicy
  def show?
    admin?
  end

  def edit?
    admin?
  end

  def new?
    admin?
  end

  def destroy?
    admin?
  end

  alias_method :create?, :new?
  alias_method :update?, :edit?
  alias_method :index?,  :show?
  alias_method :delete?, :destroy?
end
