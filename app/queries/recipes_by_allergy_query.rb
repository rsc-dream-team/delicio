# frozen_string_literal: true

class RecipesByAllergyQuery < RecipesByDietQuery
  def initialize(allergy_id)
    @allergy_id = allergy_id
  end

  private

  attr_reader :allergy_id

  def filter_by_restrictions(recipes)
    recipes.where(restrictions: { restrictable_id:   allergy_id,
                                  restrictable_type: 'Allergy' })
  end
end
