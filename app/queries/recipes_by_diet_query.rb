# frozen_string_literal: true

class RecipesByDietQuery
  def initialize(diet_id)
    @diet_id = diet_id
  end

  def call
    Recipe.includes(:products)
      .where.not(id: filter_by_restrictions(recipes_joins_restrictions))
  end

  def recipes_count
    call.size
  end

  private

  attr_reader :diet_id

  def recipes_joins_restrictions
    Recipe.joins(products: :restrictions)
  end

  def filter_by_restrictions(recipes)
    recipes.where(restrictions: { restrictable_id:   diet_id,
                                  restrictable_type: 'Diet' })
  end
end
