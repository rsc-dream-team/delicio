# frozen_string_literal: true

# == Schema Information
#
# Table name: products
#
#  id                 :bigint           not null, primary key
#  category           :integer
#  image_content_type :string
#  image_file_name    :string
#  image_file_size    :integer
#  image_updated_at   :datetime
#  kal                :integer
#  name               :string
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#

class Product < ApplicationRecord
  include ImageUploadable

  MAX_NAME_LENGTH = 30
  CATEGORIES      = %i[plant_products animal_products meat fish vegetables fruits mushrooms].freeze

  has_many :ingredients, dependent: :destroy
  has_many :recipes, through: :ingredients
  has_many :restrictions

  validates :name, :category, :kal, presence: true
  validates :name, length: { maximum: MAX_NAME_LENGTH }

  enum category: CATEGORIES
end
