# frozen_string_literal: true

# == Schema Information
#
# Table name: diets
#
#  id                 :bigint           not null, primary key
#  description        :string
#  image_content_type :string
#  image_file_name    :string
#  image_file_size    :integer
#  image_updated_at   :datetime
#  name               :string
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  user_id            :bigint
#
# Indexes
#
#  index_diets_on_user_id  (user_id)
#
# Foreign Keys
#
#  fk_rails_...  (user_id => users.id)
#

class Diet < ApplicationRecord
  include ImageUploadable

  NAME_MAX_LENGTH        = 50
  DESCRIPTION_MAX_LENGTH = 1_000

  has_many :restrictions, as: :restrictable
  has_many :recommendations, as: :recommendationable

  belongs_to :author, class_name: User.name, foreign_key: :user_id

  accepts_nested_attributes_for :restrictions, reject_if: :all_blank, allow_destroy: true
  accepts_nested_attributes_for :recommendations, reject_if: :all_blank, allow_destroy: true

  validates :name, :description, :user_id, presence: true
  validates :name,        length: { maximum: NAME_MAX_LENGTH }
  validates :description, length: { maximum: DESCRIPTION_MAX_LENGTH }
end
