# frozen_string_literal: true

# == Schema Information
#
# Table name: feedbacks
#
#  id         :bigint           not null, primary key
#  email      :string
#  message    :string
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Feedback < ApplicationRecord
  validates :name, :email, :message, presence: true
  validates_format_of :email, with: %r{\A[^@\s]+@([^@\s]+\.)+[^@\s]+\z}
end
