# frozen_string_literal: true

# == Schema Information
#
# Table name: allergies
#
#  id                 :bigint           not null, primary key
#  description        :string
#  image_content_type :string
#  image_file_name    :string
#  image_file_size    :integer
#  image_updated_at   :datetime
#  name               :string
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#

class Allergy < ApplicationRecord
  include ImageUploadable

  NAME_MAX_LENGTH        = 50
  DESCRIPTION_MAX_LENGTH = 1_000

  has_many :restrictions, as: :restrictable

  accepts_nested_attributes_for :restrictions, reject_if: :all_blank, allow_destroy: true

  validates :name, :description, presence: true

  validates :name,        length: { maximum: NAME_MAX_LENGTH }
  validates :description, length: { maximum: DESCRIPTION_MAX_LENGTH }
end
