# frozen_string_literal: true

module ImageUploadable
  extend ActiveSupport::Concern

  included do
    has_attached_file :image, styles: {
      large:  '512x512>',
      medium: '256x256>',
      small:  '128x128>',
      tiny:   '64x64>'
    }

    validates_attachment_content_type :image, content_type: %r{\Aimage/.*\Z}
  end
end
